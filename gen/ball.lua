Ball = {}
Ball.__index = Ball

Ball.image = Ball.image or love.graphics.newImage("gen/ball.png")
Ball.sound = love.audio.newSource("gen/laser.wav")

function Ball:new(x, y)
  local self = {
    x = x,
    y = y,
    speed = 10
  }

  -- Play sound.
  Ball.sound:play()

  return setmetatable(self, Ball)
end

function Ball:update(_dt)
  self.y = self.y - self.speed
end

function Ball:draw()
  love.graphics.draw(Ball.image, self.x, self.y, 0, 0.2, 0.2)
end

return Ball