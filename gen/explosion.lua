Explosion = {}
Explosion.__index = Explosion

Explosion.image = love.graphics.newImage("gen/explosion.png")
Explosion.sound = love.audio.newSource("gen/explosion_16.wav")
Explosion.frames = {}

local function init()
  local currentFrame = 1
  local frame = 1

  for y = 0, 4 do
    for x = 0, 4 do
      Explosion.frames[frame] = love.graphics.newQuad(x * 64, y * 64, 64, 64, Explosion.image:getDimensions())
      frame = frame + 1
    end
  end
end

init()

function Explosion:new(x, y)
  local self = {
    x = x,
    y = y,
    currentFrame = 1,
    accDt = 0,
    ended = false
  }

  -- Play sound.
  Explosion.sound:play()
    
  return setmetatable(self, Explosion)
end

function Explosion:update(dt)
  self.accDt = self.accDt  + dt
  if self.accDt > 0.03 then
    self.currentFrame = self.currentFrame + 1
    if self.currentFrame >= 25 then
      self.ended = true
    end
    self.accDt = 0
  end
end

function Explosion:draw()
  if not self.ended then
    love.graphics.draw(Explosion.image, Explosion.frames[self.currentFrame], self.x, self.y, 0, 1, 1)
  end
end

return Explosion