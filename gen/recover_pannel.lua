local RecoverPannel = {}
RecoverPannel.__index = RecoverPannel

function RecoverPannel:new(text)
  return setmetatable({text = text}, RecoverPannel)
end

function RecoverPannel:draw()
  love.graphics.print({{255, 255, 255}, self.text}, 270, 270, 0, 3, 3)
end

return RecoverPannel
