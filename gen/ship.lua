local Ball = require("gen.ball")

local Ship = {}
Ship.__index = Ship

Ship.image = Ship.image or love.graphics.newImage("gen/ship.png")
Ship.SPEED = 8
Ship.MIN_X = 0; Ship.MAX_X = 730
Ship.MIN_Y = 0; Ship.MAX_Y = 530

function Ship:new()
  local self = {
    currentFrame = 0,
    x = 400,
    y = 530,
    balls = {},
    frames = {}
  }

  for i = 0,0 do
    self.frames[i] = love.graphics.newQuad(64 * i, 0, 255, 255, Ship.image:getDimensions())
  end

  return setmetatable(self, Ship)
end

function Ship:draw()
  love.graphics.draw(Ship.image, self.frames[self.currentFrame], self.x, self.y, 0, 0.3, 0.3)
  for _, ball in ipairs(self.balls) do
    ball:draw()
  end
end

function Ship:update(dt, keys)
  if keys["right"] then
    self.x = self.x + Ship.SPEED
  elseif keys["left"] then
    self.x = self.x - Ship.SPEED
  end

  if keys["up"] then
    self.y = self.y - Ship.SPEED
  elseif keys["down"] then
    self.y = self.y + Ship.SPEED
  end

  if (keys["space"] or keys["f"]) and #self.balls < 10 then
    table.insert(self.balls, Ball:new(self.x + 28, self.y - 15))
    keys["space"] = false
    keys["f"] = false
  end

  if self.x < Ship.MIN_X then self.x = Ship.MIN_X else if self.x > Ship.MAX_X then self.x = Ship.MAX_X end end
  if self.y < Ship.MIN_Y then self.y = Ship.MIN_Y else if self.y > Ship.MAX_Y then self.y = Ship.MAX_Y end end

  _updateBalls(self.balls)
  self.balls = _removeBalls(self.balls)
end

function _updateBalls(balls)
  for _, ball in ipairs(balls) do
    ball:update(dt)
  end
end

function _removeBalls(balls)
  local ret = {}

  for _, ball in ipairs(balls) do
    if ball.y > -10 then table.insert(ret, ball) end
  end

  return ret
end

return Ship
