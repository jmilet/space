local Star = {}
Star.__index = Star

Star.image = love.graphics.newImage("gen/star.png")

function Star:new()
  local self = {
    x = math.random(1, 800),
    y = math.random(1, 600),
    sx = math.min(math.random(), 0.1),
    sy = sx
  }

  return setmetatable(self, Star)
end

function Star:update()
  self.y = self.y + 1

  -- If the stars goes out of screen, start over at different horizontal position
  if self.y > 600 then
    self.y = 0
    self.x = math.random(1, 800)
  end
end

function Star:draw()
  love.graphics.draw(Star.image, self.x, self.y, 0, self.sx, self.sy)
end

return Star
