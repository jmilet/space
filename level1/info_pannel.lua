InfoPannel = {}
InfoPannel.__index = InfoPannel

function InfoPannel:new(level, user)
  return setmetatable({level = level, user = user}, InfoPannel)
end

function InfoPannel:draw()
  love.graphics.print({{0, 255, 0}, "Ship: x=" .. tostring(self.level.ship.x) .. ", y=" .. tostring(self.level.ship.y)}, 10, 10)
  love.graphics.print({{0, 255, 0}, "Balls=" .. #self.level.ship.balls}, 10, 25)
  love.graphics.print({{0, 255, 0}, "Enemies=" .. #self.level.enemies}, 10, 40)
  love.graphics.print({{0, 255, 0}, "Explosions=" .. #self.level.explosions}, 10, 55)
  love.graphics.print({{0, 255, 0}, "user.lifes=" .. self.user.lifes}, 10, 70)
  love.graphics.print({{0, 255, 0}, "user.points=" .. self.user.points}, 10, 85)
end

return InfoPannel