local Ship = require("gen.ship")
local ShipEnemy = require("level1.ship_enemy")
local Star = require("gen.star")
local Explosion = require("gen.explosion")
local InfoPannel = require("level1.info_pannel")
local RecoverPannel = require("gen.recover_pannel")

-- Private functions.

local function _createObjects(constructor, numElements)
  local objects = {}

  for i = 1, numElements do
    objects[i] = constructor()
  end

  return objects
end

local function _updateObjects(objects, dt)
  for _, object in ipairs(objects) do
    object:update(dt)
  end
end

local function _drawObjects(objects)
  for _, object in ipairs(objects) do
    object:draw()
  end
end

local function _checkBallCollisions(user, balls, enemies, explosions)
  for b = #balls, 1, -1 do
    ball = balls[b]
    for e = #enemies, 1, -1 do
      local enemy = enemies[e]
      if math.abs(ball.x - enemy.x) < 40 and math.abs(ball.y - enemy.y) < 40 then
        table.insert(explosions, Explosion:new(enemy.x, enemy.y))
        table.remove(enemies, e)
        table.remove(balls, b)
        user.points = user.points + 100
      end
    end
  end
end

local function _purgeEndedExplosions(explosions)
  for i = #explosions, 1, -1 do
    local explosion = explosions[i]
    if explosion.ended then
      table.remove(explosions, i)
    end
  end
end

local function _enemyAndShipCollision(enemies, ship, explosions)
  for _, enemy in ipairs(enemies) do
    if math.abs(enemy.x - ship.x) < 60 and math.abs(enemy.y - ship.y) < 60 then
      return true
    end
  end
end

-- Level class.

Level1 = {}
Level1.__index = Level1

Level1.musicLevel1 = love.audio.newSource("level1/Edward_Shallow_-_01_-_The_Infinite_Railroad.mp3")


function Level1:new(user)
  local self = {}

  self.user = user
  self.stars = _createObjects(Star.new, 30)
  self.enemies = _createObjects(ShipEnemy.new, 10)
  self.explosions = {}
  self.ship = Ship:new()
  self.infoPannel = InfoPannel:new(self, user)
  self.recoverPannel = RecoverPannel:new("PRESS SPACE KEY")
  self.status = "normal"

  return setmetatable(self, Level1)
end

function Level1:playMusic()
  Level1.musicLevel1:play()
end

function Level1:stopMusic()
  Level1.musicLevel1:stop()
end

function Level1:draw()
  _drawObjects(self.stars)

  if self.status == "normal" then
    _drawObjects(self.enemies)
    _drawObjects(self.explosions)
    self.ship:draw()
  else
    _drawObjects(self.explosions)
    self.recoverPannel:draw()
  end

  self.infoPannel:draw()
end

function Level1:update(dt, keys)
  _updateObjects(self.stars, dt)

  if self.status == "normal" then
    self:_updateStatusNormal(dt, keys)
  elseif self.status == "recover_mode" then
    self:_updateStatusRecoverMode(dt, keys)
  else
    self:_updateStatusWaitingForKey(dt, keys)
  end
end

function Level1:incLevel()
  -- -1: previous level
  -- 0: no change
  -- 1: next level
  return 0
end

function Level1:_updateStatusNormal(dt, keys)
  _updateObjects(self.enemies, dt)
  _updateObjects(self.explosions, dt)
  self.ship:update(dt, keys)

  _checkBallCollisions(self.user, self.ship.balls, self.enemies, self.explosions)
  _purgeEndedExplosions(self.explosions)

  if _enemyAndShipCollision(self.enemies, self.ship, self.explosions) then
    table.insert(self.explosions, Explosion:new(self.ship.x, self.ship.y))
    self.user.lifes = self.user.lifes - 1
    self.status = "recover_mode"
  end
end

function Level1:_updateStatusRecoverMode(dt, keys)
  if self.status == "recover_mode" then
    self.enemies = {}
    _updateObjects(self.explosions, dt)
    _purgeEndedExplosions(self.explosions)
    if #self.explosions == 0 then
      self.status = "waiting_for_key"
    end
  end
end

function Level1:_updateStatusWaitingForKey(dt, keys)
  if keys["space"] then
    keys["space"] = false
    self.enemies = _createObjects(ShipEnemy.new, 10)
    self.status = "normal"
  end
end

return Level1
