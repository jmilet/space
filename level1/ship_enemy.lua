local ShipEnemy = {}
ShipEnemy.__index = ShipEnemy

ShipEnemy.image = love.graphics.newImage("level1/ship_enemy.png")

function ShipEnemy:new()
  local self = {
    currentFrame = 0,
    accDt = 0,
    x = math.random(0, 700),
    y = math.random(0, 400),
    speedX = math.random(-4, 4),
    speedY = math.random(-2, 2)
  }  
  if self.speedX == 0 then self.speedX = 4 end
  if self.speedY == 0 then self.speedY = 2 end

  return setmetatable(self, ShipEnemy)
end

function ShipEnemy:draw()
  love.graphics.draw(ShipEnemy.image, self.x, self.y, 0, 0.18, 0.18)
end

function ShipEnemy:update(dt)
  if self.x < 0 or self.x > 700 or math.random(1, 100) == 75 then
    self.speedX = self.speedX * -1
  end

  if self.y < 0 or self.y > 500 or math.random(1, 100) == 75 then
    self.speedY = self.speedY * -1
  end

  self.x = self.x + self.speedX
  self.y = self.y + self.speedY
end

return ShipEnemy
