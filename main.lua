Level1 = require("level1.level1")
RecoverPannel = require("gen.recover_pannel")
User = require("model.user")

local levels, currentLevel, levels
local keys = {up = false, down = false, left = false, right = false, space = false, f = false, p = false}
local pausePannel = RecoverPannel:new("PRESS SPACE KEY")
local paused = false
local user = User:new()

function love.load()
  math.randomseed(os.time())

  love.graphics.setBackgroundColor(0, 0, 0)

  levels = {Level1:new(user)}
  currentLevel = 1
  levels[currentLevel]:playMusic()
end

function love.draw()
  levels[currentLevel]:draw()
  if paused then
    pausePannel:draw()
  end
end

function love.keypressed(key)
  if keys[key] ~= nil then
    keys[key] = true
  end
end

function love.keyreleased(key)
  if keys[key] ~= nil then
    keys[key] = false
  end
end

function love.update(dt)
  local currentLevel = levels[currentLevel]

  if not paused then
    if keys["p"] then
      paused = true
      currentLevel:stopMusic()
    end
  elseif keys["space"] then
    paused = false
    keys["space"] = false
    currentLevel:playMusic()
  end


  if not paused then
    currentLevel:update(dt, keys)

    incLevel = currentLevel:incLevel()
    if incLevel ~= 0 then
      currentLevel[currentLevel].stopMusic()
      currentLevel = currentLevel + incLevel
      currentLevel[currentLevel].playMusic()
    end
  end
end
