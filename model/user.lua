local User = {}
User.__index = User

function User:new()
  self = {
    lifes = 3,
    points = 0
  }

  return setmetatable(self, User)
end

return User
